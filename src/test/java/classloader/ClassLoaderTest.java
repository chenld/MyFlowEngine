package classloader;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedHashSet;
import java.util.Set;

public class ClassLoaderTest {
	@SuppressWarnings({ "rawtypes", "resource" })
	public static void main(String[] args) throws ClassNotFoundException, IOException {
		Thread thread = Thread.currentThread();
		ClassLoader classLoader = thread.getContextClassLoader();
		System.out.println(classLoader);
		//
		ClassLoader parent = classLoader;
		parent = parent.getParent();
		while (null != parent) {
			System.out.println(parent);
			parent = parent.getParent();
		}
		// 实际加载一个jar包来测试
		Set<URL> set = new LinkedHashSet<URL>();
		set.add(new URL("file:///C:/Users/ww/Desktop/lion-client-1.1.5-plugin.jar"));
		URL[] array = set.toArray(new URL[set.size()]);
		URLClassLoader uLoader = new URLClassLoader(array);
		thread.setContextClassLoader(uLoader);
		System.out.println(uLoader);
		System.out.println(uLoader.getParent());
		// 加载一个类
		Class clazz = uLoader.loadClass("com.dianping.lion.Environment");
		System.out.println(clazz.getClassLoader());
		clazz = uLoader.loadClass("java.lang.Integer");
		System.out.println(clazz.getClassLoader());
		clazz = uLoader.loadClass("classloader.ClassLoaderTest");
		System.out.println(clazz.getClassLoader());
		//
		System.out.println("--------------------");
		uLoader = new URLClassLoader(array);
		clazz = uLoader.loadClass("com.dianping.lion.Environment");
		System.out.println(clazz.getClassLoader());
		//
		uLoader = new URLClassLoader(array);
		clazz = uLoader.loadClass("com.dianping.lion.Environment");
		System.out.println(clazz.getClassLoader());
		uLoader.close();
		

	}
}
