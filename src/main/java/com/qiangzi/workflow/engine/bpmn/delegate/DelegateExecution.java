package com.qiangzi.workflow.engine.bpmn.delegate;

public interface DelegateExecution {

	@SuppressWarnings("rawtypes")
	public void setVariable(String varName, Object value, Class valueClass) throws Exception;

	public Object getVariable(String varName);

}
