package com.qiangzi.workflow.engine.bpmn.event;

import org.dom4j.Element;

public class StartEvent extends Event {

	public void parse(Element element) throws Exception {
		super.parse(element);
	}

	public Object copy() {
		StartEvent startEvent = new StartEvent();
		copy(startEvent);
		return startEvent;
	}

	protected void copy(Object obj) {
		super.copy(obj);
	}

}
