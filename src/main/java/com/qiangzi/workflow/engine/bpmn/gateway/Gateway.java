package com.qiangzi.workflow.engine.bpmn.gateway;

import org.dom4j.Element;

import com.qiangzi.workflow.engine.bpmn.base.Node;

public abstract class Gateway extends Node {

	public void parse(Element element) throws Exception {
		super.parse(element);
	}

	protected void copy(Object obj) {
		super.copy(obj);
	}

}
