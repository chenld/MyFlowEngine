package com.qiangzi.workflow.engine.bpmn.gateway;

import org.dom4j.Element;

public class InclusiveGateway extends Gateway {

	public void parse(Element element) throws Exception {
		super.parse(element);
	}

	public Object copy() {
		InclusiveGateway gateway = new InclusiveGateway();
		copy(gateway);
		return gateway;
	}

	protected void copy(Object obj) {
		super.copy(obj);
	}

}
