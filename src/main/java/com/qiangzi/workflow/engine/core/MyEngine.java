package com.qiangzi.workflow.engine.core;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.qiangzi.workflow.engine.bpmn.base.ProcessDefinition;
import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;
import com.qiangzi.workflow.engine.service.RepositoryService;
import com.qiangzi.workflow.engine.service.RuntimeService;
import com.qiangzi.workflow.engine.service.XmlParserService;
import com.qiangzi.workflow.engine.service.impl.Dom4JXmlParserService;
import com.qiangzi.workflow.engine.service.impl.MemoryRepositoryService;
import com.qiangzi.workflow.engine.service.impl.MemoryRuntimeService;

import lombok.Data;

@Data
@Component
public class MyEngine implements Engine, ApplicationContextAware {

    // private static MyEngine INSTANCE = new MyEngine();

    private XmlParserService   xmlParserService;
    private RepositoryService  repositoryService;
    private RuntimeService     runtimeService;

    //后面要提取spring bean
    private ApplicationContext applicationContext;

    public MyEngine() {

        xmlParserService = new Dom4JXmlParserService();
        repositoryService = new MemoryRepositoryService();
        runtimeService = new MemoryRuntimeService();

    }

    @Override
    public ProcessDefinition parse(Resource resource) throws Exception {

        return xmlParserService.parse(resource);

    }

    @Override
    public ProcessInstance deploy(ProcessDefinition processDefinition) throws Exception {

        return repositoryService.deploy(processDefinition);

    }

    @Override
    public void run(ProcessInstance processInstance) throws Exception {

        this.runtimeService.run(processInstance, applicationContext);

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        this.applicationContext = applicationContext;

    }

    // public static MyEngine getInstance() {
    //
    // return INSTANCE;
    //
    // }

}
